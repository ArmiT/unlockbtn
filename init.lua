config = require("config")

function receive(conn, payload)
    print(payload) 
    
    str = string.match(payload, "key=(%a+)")

    if(str == "thecoolunlockbtn") then

        print("success")
        gpio.write(0, gpio.HIGH)
        tmr.alarm(2, 1000, tmr.ALARM_AUTO, function() 
        
            tmr.stop(2)
            gpio.write(0, gpio.LOW)
            conn:close()
        end)
    else
        print("error")
    end
    
    local content=[[
        <!DOCTYPE html>
            <html>
                <head>
                    <link rel='icon' type='image/png' href='http://nodemcu.com/favicon.png' />
                </head>
                <body>
                    <center><a href="/?key=thecoolunlockbtn"><h1>Speak, friend, and enter</h1></a></center>
                </body>
            </html>]]
    local contentLength=string.len(content)

    conn:on("sent", function(sck) sck:close() end)
    conn:send("HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nContent-Length:" .. contentLength .. "\r\n\r\n" .. content)
end

function connection(conn) 
    conn:on("receive", receive)
end

function start_server()

    srv=net.createServer(net.TCP, 1) 
    srv:listen(52632, connection)
    
end


function wifi_init()

    wifi.setmode(wifi.STATION)
    wifi.sta.config(config.wifi.ssid, config.wifi.pass)
    
--    wifi.sta.setip({
--        ip="192.168.1.108",
--        netmask="255.255.255.0",
--        gateway="192.168.0.1"
--    })
    
    wifi.sta.connect()        

    print("Connecting")
    
    tmr.alarm(1, 1000, tmr.ALARM_AUTO, function() 

        if wifi.sta.getip() ~= nil then

            tmr.stop(1)     
            print("\n===")
            print("\n mode: "..wifi.getmode())
            print("\n ip: "..wifi.sta.getip())
            
            start_server()
        else
            print(".")
            tmr.start(1) 
        end
    end) 
end


gpio.mode(0, gpio.OUTPUT)
gpio.write(0, gpio.LOW)

wifi_init()
